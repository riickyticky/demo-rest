package com.demo.latam.helpers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.demo.latam.entities.poemist.Poemist;

@Service
public class RestTemplateHelpers {
	
	private RestTemplate restTemplate;
	
	private RestTemplateHelpers() {
		restTemplate = new RestTemplate();
	}
	
	public ResponseEntity<Poemist[]> getObject(final String uriBase, final String endPoint)
    {
		Poemist[] poem;
		ResponseEntity<Poemist[]> rs;
		
		String uriPoemist = uriBase + endPoint;
         
		try {
			poem = restTemplate.getForObject(uriPoemist, Poemist[].class);
			rs = new ResponseEntity<>(poem, HttpStatus.OK);
		} catch (Exception e) {
			rs = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
   
        return rs;

    }
	
	
}
