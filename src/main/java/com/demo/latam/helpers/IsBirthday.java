package com.demo.latam.helpers;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import org.joda.time.Years;

public class IsBirthday {
	
	private IsBirthday() {}
	
	public static int knowAge(LocalDate dateOfBirthday){
		
		org.joda.time.LocalDate dateOfBirthdayJoda = new org.joda.time.LocalDate(dateOfBirthday.getYear(), dateOfBirthday.getMonthValue(), dateOfBirthday.getDayOfMonth());
		org.joda.time.LocalDate toDay = new org.joda.time.LocalDate();
		Years year = Years.yearsBetween(dateOfBirthdayJoda, toDay);
		
		return year.getYears();
    }
	
	public static Boolean isBirthday(LocalDate dateOfBirthday)  {

		
		LocalDate hoy = LocalDate.now();
		
        return (hoy.getMonthValue() == dateOfBirthday.getMonthValue() && hoy.getDayOfMonth() == dateOfBirthday.getDayOfMonth())?true:false;
        
        
    }
	
	public static Date asDate(LocalDate localDate) {
	    return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}
	
	public static Object remainingDays(LocalDate dateOfBirthday) {
		
		LocalDate today = LocalDate.now();

        LocalDate nextBDay = dateOfBirthday.withYear(today.getYear());

        if (nextBDay.isBefore(today) || nextBDay.isEqual(today)) {
            nextBDay = nextBDay.plusYears(1);
        }

        Period p = Period.between(today, nextBDay);
	    return "Te quedan " + p.getMonths() + " meses y " +
                p.getDays() + " días hasta tu proximo cumpleaños."  ;
	}
	
}
