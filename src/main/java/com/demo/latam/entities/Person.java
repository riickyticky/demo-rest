package com.demo.latam.entities;

import java.time.LocalDate;

public class Person {
	
	Long id;
	String fullName;
	LocalDate dateOfBirthday;
	Integer age;
	String message;
	
	public Person(Long id, String fullName, LocalDate dateOfBirthday, Integer age, String message) {
		this.id = id;
		this.fullName = fullName;
		this.dateOfBirthday = dateOfBirthday;
		this.age = age;
		this.message = message;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", fullName=" + fullName + ", dateOfBirthday=" + dateOfBirthday + ", age=" + age
				+ ", message=" + message + "]";
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public LocalDate getDateOfBirthday() {
		return dateOfBirthday;
	}
	public void setDateOfBirthday(LocalDate dateOfBirthday) {
		this.dateOfBirthday = dateOfBirthday;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
