package com.demo.latam.entities;

import java.time.LocalDate;

public class RequestPerson {
	
	private String fullName;
	private LocalDate dateOfBirthday;
	
	@Override
	public String toString() {
		return "RequestPerson [fullName=" + fullName + ", dateOfBirthday=" + dateOfBirthday + "]";
	}

	public RequestPerson(String fullName, LocalDate dateOfBirthday) {
		this.fullName = fullName;
		this.dateOfBirthday = dateOfBirthday;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public LocalDate getDateOfBirthday() {
		return dateOfBirthday;
	}
	public void setDateOfBirthday(LocalDate dateOfBirthday) {
		this.dateOfBirthday = dateOfBirthday;
	}
	
	
}
