package com.demo.latam.services;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.latam.constants.EndPoints;
import com.demo.latam.entities.RequestPerson;
import com.demo.latam.entities.poemist.Poemist;
import com.demo.latam.helpers.IsBirthday;
import com.demo.latam.helpers.RestTemplateHelpers;

@Service
public class PersonService {
	
	@Value("${api.poemist.url}")
	private String urlApiPoem;
	
	@Autowired
	private RestTemplateHelpers restTemplateHelpers;

	public ResponseEntity<HashMap<String, Object>> birthdayLogic(RequestPerson requestPerson) {
		ResponseEntity<HashMap<String, Object>> rs;
		HashMap<String, Object> personCustom = new HashMap<>();
		
		String personName;
		String lastName;
		
		if(requestPerson.getFullName().length() > 25) {
			personName = requestPerson.getFullName().split(" ")[0];
			lastName = requestPerson.getFullName().split(" ")[2];
		}else {
			personCustom.put("error", "Ingrese Nombre completo, Ej: Ramon Julian Perez Fernandez");
			rs = new ResponseEntity<HashMap<String,Object>>(personCustom, HttpStatus.INTERNAL_SERVER_ERROR);
			return rs;
		}

		
		personCustom.put("personName", personName);
		personCustom.put("personLastname", lastName);
		personCustom.put("dateOfBirthday", requestPerson.getDateOfBirthday()); 
		personCustom.put("personAge", IsBirthday.knowAge(requestPerson.getDateOfBirthday()));

		if(Boolean.TRUE.equals(IsBirthday.isBirthday(requestPerson.getDateOfBirthday()))){
			
				
				try {
					for(Poemist poemist: getAllPoemist().getBody()) {
						personCustom.put("birthdayCongratulations", poemist.getContent());
					}	
				} catch (Exception e) {
					personCustom.put("birthdayCongratulations", "Felicidades por su cumpleaños, pero "
									+ "tenemos problemas internos con los poemas, vuelva a intentar mas tarde");
				}
					
			
		}else {
			personCustom.put("remainingDays", IsBirthday.remainingDays(requestPerson.getDateOfBirthday()));
		}
		rs = new ResponseEntity<HashMap<String, Object>>(personCustom, HttpStatus.OK);
		return rs;
	}
	
	public ResponseEntity<Poemist[]> getAllPoemist() {
		return restTemplateHelpers.getObject(urlApiPoem, EndPoints.URI_POEMIST);
	}
	
	
}

