package com.demo.latam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.latam.entities.RequestPerson;
import com.demo.latam.services.PersonService;

@RestController
@RequestMapping("/api")
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	@CrossOrigin
	@PostMapping(value = "birthday-logic")
	public Object sendDataPerson(@RequestBody RequestPerson requestPerson) {
		
		ResponseEntity<Object> rs;
		try {
			rs = new ResponseEntity<>(this.personService.birthdayLogic(requestPerson), HttpStatus.OK);
		} catch (Exception e) {
			rs = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return rs.getBody();
		
	}
}
