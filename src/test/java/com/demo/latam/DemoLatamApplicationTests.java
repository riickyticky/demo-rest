package com.demo.latam;

import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.demo.latam.entities.RequestPerson;
import com.demo.latam.services.PersonService;

@RunWith(SpringRunner.class)
@SpringBootTest
class DemoLatamApplicationTests {
	
	
	@Autowired
	private PersonService personService;

	
	@Test
	void testLogicBirthday() {
		
		LocalDate dateOfBirthday = LocalDate.now();
		
		assertNotNull(this.personService.birthdayLogic(new RequestPerson("Ricardo Esteban Muñoz Hernandez", dateOfBirthday)));
}

}
