# demo-rest

API Rest, Desafío Latam

## Requerimientos previos para la ejecución de la API:
    .- Ultima versión Maven .
    .- Jdk 1.8 + .
    .- Configuración de variables ambientes del sistema (windows, linux, mac) .

## Para la ejecución del proyecto:
    .- En la raíz del proyecto escribir: mvn spring-boot:run .
    .- Se puede ejecutar con "Run" en algun IDE .

## Verificar endpoints:
    .- http://localhost:8081/ || http://127.0.0.1:8081/
    
## Verificar endpoints mediante Swagger:
    .- http://localhost:8081/swagger-ui.html#/ || http://127.0.0.1:8081/swagger-ui.html#/
    
## Endpoints a utilizar por el front-end:
    .- http://127.0.0.1:8081/api/birthday-logic
    .- Recibe por parametro un json de este estilo:
                                                        {
                                                          "dateOfBirthday": "2020-06-19",
                                                          "fullName": "Fabian Jorge Medina Cisterna"
                                                        }
